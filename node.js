const base = require('./base')

const node = JSON.parse(JSON.stringify(base))

node.env = {
  node: true
}

module.exports = node