const base = require('./base')

const react = JSON.parse(JSON.stringify(base))

react.plugins.push('react')

react.env = {
  browser: true,
  es6: true
}

Object.assign(react.parserOptions.ecmaFeatures, {
  jsx: true,
  experimentalDecorators: true
})

Object.assign(react.rules, {
  "react/jsx-uses-vars": 2,
  "react/jsx-uses-react": 2,
  "react/jsx-wrap-multilines": [1, {
    "declaration": "parens-new-line",
    "assignment": "parens-new-line",
    "return": "parens-new-line",
    "arrow": "parens-new-line",
    "condition": "parens-new-line",
    "logical": "parens-new-line",
    "prop": "parens-new-line"
  }],
  "react/jsx-tag-spacing": [1, {
    "closingSlash": "never",
    "beforeSelfClosing": "never",
    "afterOpening": "never",
    "beforeClosing": "never"
  }]
})

module.exports = react